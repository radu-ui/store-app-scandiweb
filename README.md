# Junior Developer Test Assignment

Frontend part:
It was designed using React library and deployed from an Express server using Heroku.
It contains different components for rendering the Product List page and the Add Product page. (their implementaion lays in the folders product-list and product-add).

Backend part:
It was designed using PHP with MySQL and hosted on 000webhost.com.
It has two endpoints: one that handles GET and POST requests and one for the DELETE request (DELETE method could be used only with premimu account in 000webhost.com so I used POST method for deletion).
Regarding storage: I've used 4 tables: 'products', 'books', 'dvds', 'furniture'. The connection between 'products' and al the other tables is done through a foreign key.

OBS: SKU input should be 8-12 characters long

