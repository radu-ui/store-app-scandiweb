<?php 
    include 'db_util.php';

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
    header("Access-Control-Allow-Headers: Content-Type");

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $conn = DatabaseUtil\DbInfo::getConnection();
        $deleteQuery = 'DELETE FROM products';
        $result = $conn->query($deleteQuery);

        if($result) {
            echo DatabaseUtil\StatusCode::$SUCCESS;
        } else {
            echo DatabaseUtil\StatusCode::$ERROR;
        }
    }
?>