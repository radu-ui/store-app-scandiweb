<?php 
    include 'db_util.php';

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: *");
    header("Access-Control-Allow-Headers: Content-Type");

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $request = json_decode(file_get_contents("php://input"));
        $status_code;
        if(class_exists('DatabaseUtil'.'\\'.$request->product_type)) {
            $conn = DatabaseUtil\DbInfo::getConnection();
            $productClass = new ReflectionClass('DatabaseUtil'.'\\'.$request->product_type);
            $product = $productClass->newInstanceArgs(array($request->product_name, $request->product_sku, $request->product_price, $request->special_attributes));
            if($product->insert($conn)) {
                $status_code = DatabaseUtil\StatusCode::$SUCCESS;
            } else {
                $status_code = DatabaseUtil\StatusCode::$ERROR;
            }
            $conn->close();
        } else {
            $status_code = DatabaseUtil\StatusCode::$ERROR;
        }
        echo $status_code;
    } else if($_SERVER['REQUEST_METHOD'] === 'GET'){
        $conn = DatabaseUtil\DbInfo::getConnection();
        $response = array(
            "books" => DatabaseUtil\Book::retrieve($conn),
            "dvds" => DatabaseUtil\DVD::retrieve($conn),
            "furniture" => DatabaseUtil\Furniture::retrieve($conn)
        );

        $conn->close();
        echo json_encode($response);;
    }

?>