<?php 
    namespace DatabaseUtil;

    interface DbOperation {
        public function insert($conn);
        public static function retrieve($conn);
    }
    
    class DbInfo {
        private static $db_host = 'localhost'; 
        private static $db_name = 'test'; 
        private static $db_user = 'db_admin';
        private static $db_pass = 'db_admin123';
  
        public static function getConnection() {
            return new \mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name);
        }
    }

    class ProductType {
        public static $BOOK = 'book';
        public static $DVD = 'dvd';
        public static $FURNITURE = 'furniture';
    }

    class StatusCode { 
        public static $SUCCESS = '200';
        public static $ERROR = '404'; 
    }

    class Product {
        private $product_name;
        private $product_sku;
        private $product_price;

        public function __construct($prod_name, $prod_sku, $prod_price) {
            $this->product_name = $prod_name;
            $this->product_price = $prod_price;
            $this->product_sku = $prod_sku;
        }

        public function getProdName() {
            return $this->product_name;
        }

        public function getProdPrice() {
            return $this->product_price;
        }

        public function getProdSku() {
            return $this->product_sku;
        }
    }

    class Book extends Product implements DbOperation{
        private $product_weight;

        public function __construct($prod_name, $prod_sku, $prod_price, $special_attrs) {
            parent::__construct($prod_name, $prod_sku, $prod_price);
            $this->product_weight = floatval($special_attrs->product_weight);    
        }

        public function getProdWeight() {
            return $this->product_weight;
        }

        public function insert($conn) {
            // Insert common attributes into products table
            $product_name = $this->getProdName();
            $product_price = floatval($this->getProdPrice());
            $product_sku = $this->getProdSku();
            $stmt = $conn->prepare('INSERT INTO products (product_name, product_price, product_sku) VALUES (?, ?, ?)');
            $stmt->bind_param('sds', $product_name, $product_price, $product_sku);
            $stmt->execute();

            // Insert book attribute into books table 
            $book_weight = $this->product_weight;
            $stmt = $conn->prepare('INSERT INTO books (book_weight, product_sku) VALUES (?, ?)');
            $stmt->bind_param('ds', $book_weight, $product_sku);
            $result = $stmt->execute();

            $stmt->close();
            return $result;
        }

        public static function retrieve($conn) {
            $joinQuery = 'SELECT products.product_name, products.product_price, products.product_sku, books.book_weight
                          FROM products, books
                          WHERE products.product_sku = books.product_sku';
            $books = array();

            if($result = $conn->query($joinQuery)) {
                while($row = $result->fetch_assoc()) {
                    $books[]= $row;
                }
                $result->free();
            }
            return $books;
        }
    }

    class DVD extends Product implements DbOperation {
        private $product_size;

        public function __construct($prod_name, $prod_sku, $prod_price, $special_attrs) {
            parent::__construct($prod_name, $prod_sku, $prod_price);
            $this->product_size = floatval($special_attrs->product_size);    
        }

        public function getProdSize() {
            return $this->product_size;
        }

        public function insert($conn) {
            // Insert common attributes into products table
            $product_name = $this->getProdName();
            $product_price = $this->getProdPrice();
            $product_sku = $this->getProdSku();
            $stmt = $conn->prepare('INSERT INTO products (product_name, product_price, product_sku) VALUES (?, ?, ?)');
            $stmt->bind_param('sds', $product_name, $product_price, $product_sku);
            $stmt->execute();

            // Insert dvd attribute into dvds table 
            $dvd_size = floatval($this->product_size);
            $stmt = $conn->prepare('INSERT INTO dvds (dvd_size, product_sku) VALUES (?, ?)');
            $stmt->bind_param('ds', $dvd_size, $product_sku);
            $result = $stmt->execute();

            $stmt->close();
            return $result;
        }

        public static function retrieve($conn) {
            $joinQuery = 'SELECT products.product_name, products.product_price, products.product_sku, dvds.dvd_size
                          FROM products, dvds
                          WHERE products.product_sku = dvds.product_sku';
            $dvds = array();

            if($result = $conn->query($joinQuery)) {
                while($row = $result->fetch_assoc()) {
                    $dvds[]= $row;
                }
                $result->free();
            }
            return $dvds;
        }
    }

    class Furniture extends Product implements DbOperation{
        private $product_width;
        private $product_height;
        private $product_length;

        public function __construct($prod_name, $prod_sku, $prod_price, $special_attrs) {
            parent::__construct($prod_name, $prod_sku, $prod_price);
            $this->product_width = floatval($special_attrs->product_width);    
            $this->product_height = floatval($special_attrs->product_height); 
            $this->product_length = floatval($special_attrs->product_length); 
        }

        public function getProdWidth() {
            return $this->product_width;
        }

        public function getProdHeight() {
            return $this->product_height;
        }

        public function getProdLength() {
            return $this->product_length;
        }

        public function insert($conn) {
            // Insert common attributes into products table
            $product_name = $this->getProdName();
            $product_price = $this->getProdPrice();
            $product_sku = $this->getProdSku();
            $stmt = $conn->prepare('INSERT INTO products (product_name, product_price, product_sku) VALUES (?, ?, ?)');
            $stmt->bind_param('sds', $product_name, $product_price, $product_sku);
            $stmt->execute();

            // Insert furniture attribute into furniture table 
            $furniture_width = floatval($this->product_width);
            $furniture_height = floatval($this->product_height);
            $furniture_length = floatval($this->product_length);
            $stmt = $conn->prepare('INSERT INTO furniture (furniture_width, furniture_height, furniture_length,  product_sku) VALUES (?, ?, ?, ?)');
            $stmt->bind_param('ddds', $furniture_width, $furniture_height, $furniture_length, $product_sku);
            $result = $stmt->execute();

            $stmt->close();
            return $result;
        }

        public static function retrieve($conn) {
            $joinQuery = 'SELECT products.product_name, products.product_price, products.product_sku, furniture.furniture_width,
                                 furniture.furniture_height, furniture.furniture_length
                          FROM products, furniture
                          WHERE products.product_sku = furniture.product_sku';
            $books = array();

            if($result = $conn->query($joinQuery)) {
                while($row = $result->fetch_assoc()) {
                    $books[]= $row;
                }
                $result->free();
            }
            return $books;
        }
    }
?>