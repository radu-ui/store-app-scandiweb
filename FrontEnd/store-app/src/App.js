import { BrowserRouter } from 'react-router-dom';
import { Routes } from './components/services/Routes';
import './styles/site.css';

function App() {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
}

export default App;
