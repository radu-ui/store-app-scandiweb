export function CommonAttributes(props) {
    const { attribute } = props;

    const handleAttributeChange = (event) => {
        attribute.attrSetter(event.target.value);
    };

    return (
            <div className="row pt-2">
                <div className="col-12 col-sm-7 col-md">
                    <label>{attribute.attrName}</label>
                </div>
                <div className="col col-sm-2 col-md-8">
                    <input id={attribute.attrId} placeholder={`Enter ${attribute.attrName} of Product`} onChange={handleAttributeChange}></input>
                </div>
            </div>
    );
}