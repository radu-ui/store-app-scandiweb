import { useRef, useState } from 'react';
import { BookForm, DVDForm, FurnitureForm } from './ProductsForm';
import { CommonAttributes } from './CommonAttributes';
import { useHistory } from 'react-router-dom';
import { Footer } from '../shared/Footer';

export function ProductAdd() {
    const endpointPOST = "https://store-app-scandiweb.000webhostapp.com/get_post_endpoint.php";

    const history = useHistory();

    const typeSwitcherRef = useRef();
    const errorDescriptionRef = useRef();

    const [weightInputValue, setWeightInputValue] = useState('');
    const [sizeInputValue, setSizeInputValue] = useState('');
    const [widthInputValue, setWidthInputValue] = useState('');
    const [heightInputValue, setHeightInputValue] = useState('');
    const [lengthInputValue, setLengthInputValue] = useState('');
    const [SKUInputValue, setSKUInputValue] = useState('');
    const [nameInputValue, setNameInputValue] = useState('');
    const [priceInputValue, setPriceInputValue] = useState('');
    const [productId, setProductId] = useState('DVD');
    const [productType, setProductType] = useState('');

    const specialAttributes = {
        "DVD": <DVDForm setParentSizeInput={setSizeInputValue} setProductType={setProductType}/>,
        "Book": <BookForm setParentWeightInput={setWeightInputValue} setProductType={setProductType}/>,
        "Furniture": <FurnitureForm setParentWidthInput={setWidthInputValue} setPareHeightInput={setHeightInputValue} setParentLengthInput={setLengthInputValue} setProductType={setProductType}/>
    }

    const commonAttributes = [
        {attrName: "SKU", attrId:"sku", attrSetter:setSKUInputValue},
        {attrName: "Name", attrId:"name", attrSetter:setNameInputValue},
        {attrName: "Price", attrId:"price", attrSetter:setPriceInputValue}
    ]

    const errorMessage = {
        emptyInputs: "Please, submit required data",
        invalidDataTypeInputs: "Please, provide the data of indicated type",
        errorToInsert: "Cannot insert. Make sure SKU is unique"
    }

    const redirectToProductList = () => {
        history.push('/');
    }

    const areEmptyInputs = () => {
        if(SKUInputValue !== '' && nameInputValue !== '' && priceInputValue !== '') {
            if((widthInputValue !== '' && heightInputValue !== '' && lengthInputValue !== '') || sizeInputValue !== '' || weightInputValue !== '' ) {
                return false;
            }
        }
        return true;
    }

    const areInvalidDataTypeInputs = () => {
        if(isNaN(priceInputValue) || isNaN(widthInputValue) || isNaN(heightInputValue) || isNaN(lengthInputValue) 
            || isNaN(sizeInputValue) || isNaN(weightInputValue) || SKUInputValue.length < 8 || 
            SKUInputValue.length > 12 || isNaN(nameInputValue) === false) {
                return true;
            }


        return false;
        }

    const areInputsValid = () => {
        if(areEmptyInputs()) {
            errorDescriptionRef.current.innerHTML = errorMessage.emptyInputs; 
            return false;
        }
        if(areInvalidDataTypeInputs()) {
            errorDescriptionRef.current.innerHTML =  errorMessage.invalidDataTypeInputs;
            return false;
        }

        return true;
    }

    const handleTypeSwitcher = () => {
        setProductId(typeSwitcherRef.current.value);
        errorDescriptionRef.current.innerHTML = "";
    }

    const handleFormSubmission = async () => {
        if(areInputsValid()) {
            const productData = {
                product_type: productType,
                product_sku: SKUInputValue,
                product_name: nameInputValue,
                product_price: priceInputValue,
                special_attributes: {
                    product_size: sizeInputValue,
                    product_weight: weightInputValue,
                    product_width: widthInputValue,
                    product_height: heightInputValue,
                    product_length: lengthInputValue
                }
            };
            const options = {
                method: 'POST',
                body: JSON.stringify(productData)
            }
            console.log(productData);
            const response = await fetch(endpointPOST, options)
            const statusCode = await response.text();
            if(statusCode === '200') {
                redirectToProductList();
            } else {
                errorDescriptionRef.current.innerHTML = errorMessage.errorToInsert;
            }
        } else {
            errorDescriptionRef.current.innerHTML = errorMessage.invalidDataTypeInputs;
        }
    }

    return (
        <>
        {/* Navigation of Product Add Page */}
        <div className="container pt-5 pb-3">
          <div id="navbar-content" className="d-flex justify-content-between">
                <div className="p-4">Product Add</div>
                <div className="d-flex">
                  <div className="p-3"><button onClick={handleFormSubmission} name="SAVE">Save</button></div>
                  <div className="p-3">
                      <button onClick={redirectToProductList} >
                        <span className="full-text">Cancel</span>
                        <span className="short-text">Quit</span>
                      </button> 
                    </div>
                </div>
          </div>
        </div>
        
        {/* Content of Product Add Page */}
        <div className="page-content">
            <div className="p-3">
                <form id="product_form">
                    <div className="container pt-3">
                        {/* Common attributes inputs: SKU, Name, Price */}
                        {commonAttributes.map((attribute, index) => {
                            return <CommonAttributes key={index} attribute={attribute}/>
                        })}
                        
                        {/* Type Switcher input */}
                        <div className="row pt-2">
                            <div className="col-12 col-sm-7 col-md">
                                <label>Type Switcher</label>
                            </div>
                            <div className="col col-sm-2 col-md-8">
                                <select id="productType" className="form-select form-select-xl" onChange={handleTypeSwitcher} ref={typeSwitcherRef}>
                                    <option value="DVD">DVD</option>
                                    <option value="Book">Book</option>
                                    <option value="Furniture">Furniture</option>
                                </select>
                            </div>
                        </div>
                        <div id="delimitation"></div>
                        
                        {/* Special attributes inputs */}
                        <div className="pt-5">
                            {specialAttributes[productId]}
                        </div>

                        {/* Error message */}
                        <div id="error-message" className="pt-2" ref={errorDescriptionRef}></div>
                    </div>
                    {/* Special attributes inputs: Size/Weight/Dimensions */}
                </form>
            </div>
            <div className="spacer"></div>
        </div>
        
        {/* Footer of Product Add Page */}
        <Footer />
        </>
    )
}