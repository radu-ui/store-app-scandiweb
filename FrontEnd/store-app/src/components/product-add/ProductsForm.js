import { useEffect } from "react";

export function BookForm(props) { 
    const { setParentWeightInput, setProductType } = props;
    const productType = 'book';
    
    useEffect(() => {
        setProductType(productType);    
    })

    const handleWeightInputChange = (event) => {
        setParentWeightInput(event.target.value);
    }

    return (
        <>
            <div className="row">
                <div className="col-12 col-sm-7 col-md">
                    <label>Weight (KG)</label>
                </div>
                <div className="col col-sm-2 col-md-8">
                    <input id="weight" placeholder="Enter Weight of Product" onChange={handleWeightInputChange}></input>
                </div>
            </div>
            <div className="row pt-2">
                <div className="col" style={{fontSize: "12px", color:'blue'}}>
                    <span>Please provide weight in KG</span>
                </div>
            </div>
        </>
    );
}

export function DVDForm(props) {
    const { setParentSizeInput, setProductType } = props;
    const productType = 'dvd';

    useEffect(() => {
        setProductType(productType);    
    })

    const handleSizeInputChange = (event) => {
        setParentSizeInput(event.target.value);
    }

    return (
        <>
            <div className="row ">
                <div className="col-12 col-sm-7 col-md">
                    <label >Size (MB)</label>
                </div>
                <div className="col col-sm-2 col-md-8">
                    <input id="size" placeholder="Enter Seize of Product" onChange={handleSizeInputChange}></input>
                </div>
            </div>
            <div className="row pt-2">
                <div className="col" style={{fontSize: "12px", color:'blue'}}>
                    <span>Please provide size in MB</span>
                </div>
            </div>
        </>
    );
}

export function FurnitureForm(props) { 
    const { setParentWidthInput, setPareHeightInput, setParentLengthInput, setProductType } = props;
    const productType = 'furniture';

    useEffect(() => {
        setProductType(productType);
    })

    const handleHeightInputChange = (event) => {
        setPareHeightInput(event.target.value);
    };
    const handleWidthInputChange = (event) => {
        setParentWidthInput(event.target.value);
    };
    const handleLengthInputChange = (event) => {
        setParentLengthInput(event.target.value);
    }; 

    return (
        <>
            <div className="row">
                <div className="col-12 col-sm-7 col-md">
                    <label>Height</label>
                </div>
                <div className="col col-sm-2 col-md-8">
                    <input id="height" placeholder="Enter Height of Product" onChange={handleHeightInputChange}></input>
                </div>
            </div>
            <div className="row pt-2">
                <div className="col-12 col-sm-7 col-md">
                    <label>Width</label>
                </div>
                <div className="col col-sm-2 col-md-8">
                    <input id="width" placeholder="Enter Width of Product" onChange={handleWidthInputChange}></input>
                </div>
            </div>
            <div className="row pt-2">
                <div className="col-12 col-sm-7 col-md">
                    <label>Length</label>
                </div>
                <div className="col col-sm-2 col-md-8">
                    <input id="length" placeholder="Enter Length of Product" onChange={handleLengthInputChange}></input>
                </div>
            </div>
            <div className="row pt-2" style={{fontSize: "12px", color:'blue'}}>
                <div className="col">
                    <span>Please provide size in width, height, length</span>
                </div>
            </div>
        </>
    );
}