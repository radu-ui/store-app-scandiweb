export function Card(props) {
    const { item, specialAttribute } = props;

    return (
        <div className="card-container col-6 col-md-4 col-lg-4 col-xl-3 pt-5">
            <div className="my-card p-2">
                <div className="rect"></div>
                <div className="">
                    <div>{item.product_sku}</div>
                    <div>{item.product_name}</div>
                    <div>{item.product_price} $</div>
                    <div>{specialAttribute}</div>
                </div>
            </div>
        </div>
    );
}