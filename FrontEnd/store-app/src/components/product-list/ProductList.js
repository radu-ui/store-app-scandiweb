import { useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { Book, Furniture, DVD } from './Products';
import { Footer } from '../shared/Footer';

export function ProductList() {
    const [bookList, setBookList] = useState([]); 
    const [DVDList, setDVDList] = useState([]);
    const [furnitureList, setFurnitureList] = useState([]);
    const endpointDELETE = 'https://store-app-scandiweb.000webhostapp.com/delete_endpoint.php';
    const endpointGET = 'https://store-app-scandiweb.000webhostapp.com/get_post_endpoint.php';

    const history = useHistory();

    const handleMassDelete = async () => {
      const options = {
          method: "POST"
        };
      const response = await fetch(endpointDELETE, options); 
      const statusCode = await response.text();
      if(statusCode === '200') {
        setFurnitureList([]);
        setBookList([]);
        setDVDList([]);
      }
    } 

    const redirectToProductAdd = () => {
        history.push('/product-add');
    }

    async function fetchData() {
      const response = await fetch(endpointGET);
      const productList = await response.json();
      setBookList(productList.books);
      setDVDList(productList.dvds);
      setFurnitureList(productList.furniture);
    }

    useEffect(() => {
      fetchData();
    }, [bookList.length, DVDList.length, furnitureList.length]);
    
    return (
      <>
        {/* Navigation of Product List Page */}
        <div className="container pt-5 pb-3">
          <div id="navbar-content" className="d-flex justify-content-between">
                <div className="p-4">Product List</div>
                <div className="d-flex">
                  <div className="p-3"><button onClick={redirectToProductAdd} name="ADD">Add</button></div>
                  <div className="p-3">
                    <button onClick={handleMassDelete} name="MASS DELETE">
                      <span className="full-text">Mass Delete</span>
                      <span className="short-text">Erase</span>
                    </button>
                  </div>
                </div>
          </div>
        </div>

        {/* Content of Product List Page */}
        <div className="page-content">
          <div className="container" id="product-list">
              <div className="row">
                <Book bookList={bookList} />
                <Furniture furnitureList={furnitureList} />
                <DVD DVDList={DVDList} />
              </div>
          </div>
          <div className="spacer"></div>
        </div>

        {/* Footer of Product List Page */}
        <Footer />
      </>
    );
}