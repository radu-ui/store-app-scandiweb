import { Card } from './Card';

export function Book(props) {
    const bookList = props.bookList;

    return (
        <>
        {
            bookList.map((book, index) => {
                return (
                    <Card key={index} item={book} specialAttribute={`Weight: ${book.book_weight}KG`} />
                );
            })
        }
        </>
    );
}

export function Furniture(props) {
    const furnitureList = props.furnitureList; 

    return (
        <>
        {
            furnitureList.map((furniture, index) => {
                return (
                    <Card key={index} item={furniture} specialAttribute={`Dimension: ${furniture.furniture_width}X${furniture.furniture_height}X${furniture.furniture_length}`} />
                );
            })
        }
        </>
    );
}

export function DVD(props) {
    const DVDList = props.DVDList;

    return (
        <>
        {
            DVDList.map((dvd, index) => {
                return (
                    <Card key={index} item={dvd} specialAttribute={`Size: ${dvd.dvd_size} MB`} />
                );
            })
        }
        </>
    );
}