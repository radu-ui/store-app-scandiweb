import { ProductList } from '../product-list/ProductList';
import { ProductAdd } from '../product-add/ProductAdd';
import { Switch, Route } from 'react-router-dom';

export function Routes() {
    return (
        <Switch>
            <Route path='/' exact component={ProductList} />
            <Route path='/product-add' component={ProductAdd} />
        </Switch>
    );
}